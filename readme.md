This tool is meant for recording streams on a local machine
and is only meant for if you do not want to support twitch but
the streamer does have other means of you donating to him or her

## USAGE
run the tool with `./TwitchRecordingTool` and choice your streamer form the list
or manually enter there username and the target folder name with option `0`

for now the tool will automatically shutdown your pc if you have insufficient rights
other wise it will just complain about the lack and leave your pc running.
We are working on making this an option.
